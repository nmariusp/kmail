# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Steve Allewell <steve.allewell@gmail.com>, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-30 01:38+0000\n"
"PO-Revision-Date: 2022-02-19 12:10+0000\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English <kde-l10n-en_gb@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.1\n"

#: addarchivemaildialog.cpp:21
#, kde-format
msgctxt "@title:window"
msgid "Modify Archive Mail"
msgstr "Modify Archive Mail"

#: addarchivemaildialog.cpp:23 archivemailwidget.cpp:274
#, kde-format
msgctxt "@title:window"
msgid "Add Archive Mail"
msgstr "Add Archive Mail"

#: addarchivemailwidget.cpp:26
#, kde-format
msgid "Archive all subfolders"
msgstr "Archive all subfolders"

#: addarchivemailwidget.cpp:36
#, fuzzy, kde-format
#| msgid "&Folder:"
msgid "Folder:"
msgstr "&Folder:"

#: addarchivemailwidget.cpp:46
#, kde-format
msgid "Format:"
msgstr "Format:"

#: addarchivemailwidget.cpp:54
#, kde-format
msgid "Path:"
msgstr "Path:"

#: addarchivemailwidget.cpp:61
#, kde-format
msgid "Backup each:"
msgstr "Backup each:"

#: addarchivemailwidget.cpp:73
#, kde-format
msgid "Maximum number of archive:"
msgstr "Maximum number of archive:"

#: addarchivemailwidget.cpp:76
#, kde-format
msgid "unlimited"
msgstr "unlimited"

#: archivemailinfo.cpp:100 archivemailinfo.cpp:116
#, kde-format
msgctxt "Start of the filename for a mail archive file"
msgid "Archive"
msgstr "Archive"

#: archivemailrangewidget.cpp:20
#, kde-format
msgid "Use Range"
msgstr ""

#: archivemailwidget.cpp:69
#, kde-format
msgid "Name"
msgstr "Name"

#: archivemailwidget.cpp:69
#, kde-format
msgid "Last archive"
msgstr "Last archive"

#: archivemailwidget.cpp:69
#, kde-format
msgid "Next archive in"
msgstr "Next archive in"

#: archivemailwidget.cpp:69
#, kde-format
msgid "Storage directory"
msgstr "Storage directory"

#: archivemailwidget.cpp:88
#, kde-format
msgid "Archive Mail Agent"
msgstr "Archive Mail Agent"

#: archivemailwidget.cpp:90
#, kde-format
msgid "Archive emails automatically."
msgstr "Archive emails automatically."

#: archivemailwidget.cpp:92
#, kde-format
msgid "Copyright (C) 2014-%1 Laurent Montel"
msgstr "Copyright (C) 2014-%1 Laurent Montel"

#: archivemailwidget.cpp:93
#, kde-format
msgid "Laurent Montel"
msgstr "Laurent Montel"

#: archivemailwidget.cpp:93
#, kde-format
msgid "Maintainer"
msgstr "Maintainer"

#: archivemailwidget.cpp:95
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Steve Allewell"

#: archivemailwidget.cpp:95
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "steve.allewell@gmail.com"

#. i18n: ectx: property (text), widget (QPushButton, addItem)
#: archivemailwidget.cpp:106 ui/archivemailwidget.ui:31
#, kde-format
msgid "Add..."
msgstr "Add..."

#: archivemailwidget.cpp:112
#, kde-format
msgid "Open Containing Folder..."
msgstr "Open Containing Folder..."

#. i18n: ectx: property (text), widget (QPushButton, deleteItem)
#: archivemailwidget.cpp:115 ui/archivemailwidget.ui:45
#, kde-format
msgid "Delete"
msgstr "Delete"

#: archivemailwidget.cpp:165
#, kde-format
msgid "Folder: %1"
msgstr "Folder: %1"

#: archivemailwidget.cpp:187
#, kde-format
msgid "Tomorrow"
msgid_plural "%1 days"
msgstr[0] "Tomorrow"
msgstr[1] "%1 days"

#: archivemailwidget.cpp:196
#, kde-format
msgid "Archive will be done %1"
msgstr "Archive will be done %1"

#: archivemailwidget.cpp:232
#, kde-format
msgid "Do you want to delete the selected items?"
msgstr "Do you want to delete the selected items?"

#: archivemailwidget.cpp:233
#, kde-format
msgctxt "@title:window"
msgid "Delete Items"
msgstr "Delete Items"

#: archivemailwidget.cpp:273
#, kde-format
msgid ""
"Cannot add a second archive for this folder. Modify the existing one instead."
msgstr ""
"Cannot add a second archive for this folder. Modify the existing one instead."

#: job/archivejob.cpp:58
#, kde-format
msgid "Directory does not exist. Please verify settings. Archive postponed."
msgstr "Directory does not exist. Please verify settings. Archive postponed."

#: job/archivejob.cpp:75
#, kde-format
msgid "Start to archive %1"
msgstr "Start to archive %1"

#. i18n: ectx: property (text), widget (QPushButton, modifyItem)
#: ui/archivemailwidget.ui:38
#, kde-format
msgid "Modify..."
msgstr "Modify..."

#: widgets/formatcombobox.cpp:14
#, kde-format
msgid "Compressed Zip Archive (.zip)"
msgstr "Compressed Zip Archive (.zip)"

#: widgets/formatcombobox.cpp:15
#, kde-format
msgid "Uncompressed Archive (.tar)"
msgstr "Uncompressed Archive (.tar)"

#: widgets/formatcombobox.cpp:16
#, kde-format
msgid "BZ2-Compressed Tar Archive (.tar.bz2)"
msgstr "BZ2-Compressed Tar Archive (.tar.bz2)"

#: widgets/formatcombobox.cpp:17
#, kde-format
msgid "GZ-Compressed Tar Archive (.tar.gz)"
msgstr "GZ-Compressed Tar Archive (.tar.gz)"

#: widgets/unitcombobox.cpp:13
#, kde-format
msgid "Days"
msgstr "Days"

#: widgets/unitcombobox.cpp:14
#, kde-format
msgid "Weeks"
msgstr "Weeks"

#: widgets/unitcombobox.cpp:15
#, kde-format
msgid "Months"
msgstr "Months"

#: widgets/unitcombobox.cpp:16
#, kde-format
msgid "Years"
msgstr "Years"

#~ msgid "Add Archive Mail"
#~ msgstr "Add Archive Mail"

#~ msgid "Remove items"
#~ msgstr "Remove items"

#~ msgid "Remove"
#~ msgstr "Remove"

#~ msgid "Archive now"
#~ msgstr "Archive now"

#~ msgid "Configure Archive Mail Agent"
#~ msgstr "Configure Archive Mail Agent"
